package com.homedecoration.common.util;

import com.biloba.common.core.constant.ConfigurationConst;
import com.biloba.common.core.util.FileUtil;
import com.biloba.common.core.util.UUIDUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by xy on 2018/7/3.
 */
public class HtmlUtil {

    public static String replaceImg(String savePath ,String imgUrl){
        String imageName = UUIDUtil.randomString(32) + ".jpg";
        savePath = savePath + imageName;
        FileOutputStream fos = null;
        BufferedInputStream bis = null;
        HttpURLConnection httpUrl = null;
        URL url;
        int BUFFER_SIZE = 1024;
        byte[] buf = new byte[BUFFER_SIZE];
        int size;
        try {
            url = new URL(imgUrl);
            httpUrl = (HttpURLConnection) url.openConnection();
            httpUrl.connect();
            bis = new BufferedInputStream(httpUrl.getInputStream());
            fos = new FileOutputStream(savePath);
            while ((size = bis.read(buf)) != -1) {
                fos.write(buf, 0, size);
            }
            fos.flush();
        } catch (IOException e) {
            savePath = null;
        }  finally {
            try {
                fos.close();
                bis.close();
                httpUrl.disconnect();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }
        }
        return savePath;
    }

    public static String parseHtml(String url) {
        String s;
        try {
            File input = new File(url);
            Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
            Element content = doc.getElementById("article");
            Elements elements = content.children();
            s = elements.toString();
        } catch (Exception e) {
            s = null;
        }
        return s;
    }

    public static String uploadHtml(String templateUrl, String content, String videoPreview) throws IOException {
        File input = new File(templateUrl);
        Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
        Element element = doc.getElementById("article");
        element.append(content);
        if (videoPreview != null) {
            Elements videos = doc.getElementsByTag("video");
            for (Element video : videos) {
                video.attr("poster", ConfigurationConst.RESOURCE_ROOT_URL + videoPreview);
            }
        }
        Elements imgs = doc.getElementsByTag("img");
        for (Element img : imgs) {
            String url = img.attr("src");
            String absolutePath = HtmlUtil.replaceImg(ConfigurationConst.RESOURCE_ROOT_PATH +  "/resources/image/html_img/", url);
            if (absolutePath != null) {
                String newUrl = absolutePath.replace(ConfigurationConst.RESOURCE_ROOT_PATH, "");
                img.attr("src", ConfigurationConst.RESOURCE_ROOT_URL + newUrl);
            }
        }
        return FileUtil.saveFile(doc.outerHtml().getBytes(), "html");
    }
}
