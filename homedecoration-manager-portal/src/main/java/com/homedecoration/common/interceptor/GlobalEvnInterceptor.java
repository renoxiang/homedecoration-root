/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.homedecoration.common.interceptor;

import com.biloba.common.core.constant.ConfigurationConst;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 环境变量拦截器
 * @Author: estn.zuo
 * @CreateTime: 2017-03-23 16:24
 */
public class GlobalEvnInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        if (request.getSession().getAttribute("RESOURCE_ROOT_URL") == null) {
            request.getSession().setAttribute("RESOURCE_ROOT_URL", ConfigurationConst.RESOURCE_ROOT_URL);
            request.getSession().setAttribute("RESOURCE_ROOT_PATH", ConfigurationConst.RESOURCE_ROOT_PATH);
        }
        return true;
    }
}
