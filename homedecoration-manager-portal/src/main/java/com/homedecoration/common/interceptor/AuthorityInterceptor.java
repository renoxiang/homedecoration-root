package com.homedecoration.common.interceptor;

import com.biloba.common.core.exception.BaseException;
import com.biloba.common.domain.enumeration.AvailableEnum;
import com.homedecoration.persist.domain.sys.Operator;
import com.homedecoration.persist.enumeration.OperatorStatusEnum;
import com.homedecoration.service.sys.OperatorService;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthorityInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private OperatorService operatorService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String operatorname = (String) SecurityUtils.getSubject().getPrincipal();
        Operator operator = operatorService.selectAllOperatorByName(operatorname);
        if (operator.getOperatorStatus().equals(OperatorStatusEnum.FROZEN)) {
            throw new BaseException("账户被冻结");
        }
        if (operator.getAvailable().equals(AvailableEnum.UNAVAILABLE)) {
            throw new BaseException("账户被删除");
        }
        return true;
    }
}
