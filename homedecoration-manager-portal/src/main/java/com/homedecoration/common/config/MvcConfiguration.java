package com.homedecoration.common.config;

import com.biloba.common.portal.formatter.StringToDateFormatter;
import com.homedecoration.common.interceptor.AuthorityInterceptor;
import com.homedecoration.common.interceptor.CommonParamInterceptor;
import com.homedecoration.common.interceptor.GlobalEvnInterceptor;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * @Description: Spring MVC 配置
 * @Author: estn.zuo
 * @CreateTime: 2016-12-15 16:44
 */
@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public AuthorityInterceptor getAuthorityInterceptor(){
        return new AuthorityInterceptor();
    }

    /**
     * 配置自定义拦截器
     * <p>
     * SignatureInterceptor
     * AuthorizationInterceptor
     * AuthInterceptor
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CommonParamInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new GlobalEvnInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(getAuthorityInterceptor()).excludePathPatterns("/login","/","/upload");
    }


    /**
     * 配置消息转换器
     * <p>
     * 统一使用json消息转换器
     *
     * @param converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        //converters.add(new JacksonObjectMapperHttpMessageConvert());
    }

    /**
     * 配置异常处理器
     *
     * @param exceptionResolvers
     */
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        //ServerExceptionResolver resolver = new ServerExceptionResolver();
        //resolver.setContentType("application/json;charset=UTF-8");
        //exceptionResolvers.add(resolver);
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }

    /**
     * 配置Date格式的参数转换器
     *
     * @param registry
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new StringToDateFormatter());
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {
            /**
             * 自定义404页面处理
             * @param container
             */
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/error/404"));
            }
        };
    }

}
