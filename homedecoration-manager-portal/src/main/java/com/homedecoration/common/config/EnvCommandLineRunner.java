/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.homedecoration.common.config;

import com.biloba.common.core.constant.ConfigurationConst;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * @Description: XrjCommandLineRunner
 * @Author: estn.zuo
 * @CreateTime: 2016-12-16 13:48
 */
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
public class EnvCommandLineRunner implements CommandLineRunner {

    @Value("${resource.root.path}")
    private String resourcesRootPath;

    @Value("${resource.root.url}")
    private String resourcesRootUrl;

    @Value("${spring.profiles.active}")
    private String systemEnvironment;

    @Override
    public void run(String... args) throws Exception {
        //初始化一些系统常量
        ConfigurationConst.RESOURCE_ROOT_PATH = resourcesRootPath;
        ConfigurationConst.RESOURCE_ROOT_URL = resourcesRootUrl;
        ConfigurationConst.SYSTEM_ENVIRONMENT = systemEnvironment;

    }
}
