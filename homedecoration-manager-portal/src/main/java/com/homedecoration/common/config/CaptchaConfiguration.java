/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.homedecoration.common.config;

import com.biloba.common.portal.captcha.JCaptchaEngine;
import com.biloba.common.portal.captcha.JCaptchaFilter;
import com.google.common.collect.ImmutableMap;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.servlet.DispatcherType;

/**
 * @Description: CaptchaConfig
 * @Author: estn.zuo
 * @CreateTime: 2017-03-28 09:48
 */
@Configuration
public class CaptchaConfiguration {

    @Bean
    public FilterRegistrationBean filterRegistration() {
        FilterRegistrationBean filterRegistration = new FilterRegistrationBean();
        filterRegistration.setFilter(filter());
        filterRegistration.setEnabled(true);
        filterRegistration.addUrlPatterns("/captcha.jpg");
        filterRegistration.setDispatcherTypes(DispatcherType.REQUEST);
        filterRegistration.setOrder(Ordered.LOWEST_PRECEDENCE);
        filterRegistration.setInitParameters(ImmutableMap.of("targetFilterLifecycle", "true"));
        return filterRegistration;
    }

    @Bean("captchaService")
    public DefaultManageableImageCaptchaService service() {
        DefaultManageableImageCaptchaService service = new DefaultManageableImageCaptchaService();
        JCaptchaEngine engine = new JCaptchaEngine();
        service.setCaptchaEngine(engine);
        service.setMinGuarantedStorageDelayInSeconds(600);
        return service;
    }

    @Bean("captchaFilter")
    public JCaptchaFilter filter() {
        JCaptchaFilter filter = new JCaptchaFilter();
        filter.setCaptchaService(service());
        return filter;
    }


}
