package com.homedecoration.common.config;

import com.biloba.common.domain.enumeration.AvailableEnum;
import com.biloba.common.portal.shiro.CredentialsMatcher;
import com.homedecoration.persist.domain.sys.Operator;
import com.homedecoration.persist.domain.sys.Role;
import com.homedecoration.persist.enumeration.OperatorStatusEnum;
import com.homedecoration.service.sys.OperatorService;
import com.homedecoration.service.sys.RoleService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component("userRealm")
public class UserRealm extends AuthorizingRealm {

    @Resource
    private OperatorService operatorService;

    @Resource
    private RoleService roleService;

    public UserRealm() {
        setName("UserRealm");
        setCredentialsMatcher(new CredentialsMatcher());
    }

    /**
     * 刷新用户权限缓存
     */
    public void clearCachedAuthorizationInfo(String operatorname) {
        Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
        if (cache != null) {
            SimplePrincipalCollection sp = new SimplePrincipalCollection(operatorname,"UserRealm");
            cache.remove(sp);
        }
    }
    //权限资源角色
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Operator operator = operatorService.selectByOperatorname(username);
        Role role = roleService.selectRoleById(operator.getRoleId());
        info.addStringPermission(role.getAuthority());
        return info;
    }

    //登录验证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String operatorname = usernamePasswordToken.getUsername();
        Operator operator = operatorService.selectAllOperatorByName(operatorname);
        if (operator == null) {
            throw new UnknownAccountException();
        }
        if (operator.getAvailable().equals(AvailableEnum.UNAVAILABLE)) {
            throw new UnknownAccountException();
        }
        if (operator.getOperatorStatus().equals(OperatorStatusEnum.FROZEN)){
            throw new DisabledAccountException();
        }
        return new SimpleAuthenticationInfo(operatorname,operator.getPassword(), getName());
    }
}