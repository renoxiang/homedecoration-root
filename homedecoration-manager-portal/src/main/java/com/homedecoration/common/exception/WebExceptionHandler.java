package com.homedecoration.common.exception;

import com.biloba.common.core.exception.BaseException;
import com.biloba.common.portal.support.DWZ;
import com.biloba.common.portal.support.DwzSupport;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Description: 自定义异常处理
 * User: yang
 * Date: 2017-06-07
 * Time: 10:50
 */
@ControllerAdvice
public class WebExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(WebExceptionHandler.class);

    @ExceptionHandler({UnauthorizedException.class })
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String processUnauthorizedException(HttpServletRequest request, UnauthorizedException e) {
        logger.error("UnauthorizedException", e);
        return DwzSupport.ajaxError("无操作权限！", DWZ.CLOSE_CURRENT);
    }

    @ExceptionHandler({BaseException.class })
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String accountDeleteException(HttpServletRequest request, HttpServletResponse response, BaseException baseException) {
        String content = baseException.getError().getErrorCode().equals("4001") ?
                "您的账号被删除，请联系管理员。可重新添加该账号" : "您的账号已停用，请联系管理员。";

        String xRequestedWith = request.getHeader("x-requested-with");
        if ("XMLHttpRequest".equals(xRequestedWith)) {
            try {
                response.setContentType("text/html" + ";charset=UTF-8");
                response.setHeader("Connection", "keep-alive");
                response.setDateHeader("Expires", 0);
                response.getWriter().write(DwzSupport.ajaxLogout(content));
                response.getWriter().flush();
            } catch (IOException io) {
                io.printStackTrace();
            }
            return null;
        }
        return "login";
    }

}
