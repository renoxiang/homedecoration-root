package com.homedecoration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60 * 60 * 6)
@SpringBootApplication
public class DecorationManagerPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DecorationManagerPortalApplication.class, args);
	}
}
