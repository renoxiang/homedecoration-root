package com.homedecoration.web.biz;

import com.biloba.common.domain.Expression;
import com.biloba.common.domain.Operation;
import com.biloba.common.domain.pojo.DomainPage;
import com.biloba.common.portal.support.DWZ;
import com.biloba.common.portal.support.DwzSupport;
import com.google.common.base.Strings;
import com.homedecoration.persist.domain.biz.CustomerRendering;
import com.homedecoration.persist.domain.biz.SampleCustomer;
import com.homedecoration.persist.vo.RenderingVO;
import com.homedecoration.service.biz.CustomerRenderingService;
import com.homedecoration.service.biz.IdentifierService;
import com.homedecoration.service.biz.SampleCustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by xy on 2018/9/30.
 */
@Controller
@RequestMapping("biz/customer")
public class SampleCustomerController {

    private static final String PATH = "biz/customer/";

    public static final String CUSTOMER_NO_PREFIX = "70";

    @InitBinder("sampleCustomer")
    private void initCustomerForBinder(WebDataBinder binder){
        binder.setFieldDefaultPrefix("sampleCustomer.");
    }

    @InitBinder("renderingVO")
    private void initVOForBinder(WebDataBinder binder){
        binder.setFieldDefaultPrefix("renderingVO.");
    }

    @Resource
    private SampleCustomerService sampleCustomerService;

    @Resource
    private IdentifierService identifierService;

    @Resource
    private CustomerRenderingService customerRenderingService;

    @RequestMapping("search")
    public String search(Model model, SampleCustomer sampleCustomer, Integer pageIndex, Integer pageSize){
        List<Expression> expressionList = new ArrayList<>();
        if (!Strings.isNullOrEmpty(sampleCustomer.getCustomerName())) {
            expressionList.add(new Expression("customerName", Operation.ALL_LIKE, sampleCustomer.getCustomerName()));
        }
        if (!Strings.isNullOrEmpty(sampleCustomer.getCustomerNo())) {
            expressionList.add(new Expression("customerNo", Operation.ALL_LIKE, sampleCustomer.getCustomerNo()));
        }
        if (Objects.nonNull(sampleCustomer.getIsTop())) {
            expressionList.add(new Expression("isTop", Operation.EQUALS, sampleCustomer.getIsTop()));
        }
        DomainPage domainPage = sampleCustomerService.selectByPage(expressionList, pageIndex, pageSize);
        model.addAttribute("domainPage", domainPage);
        model.addAttribute("sampleCustomer", sampleCustomer);
        return PATH + "list";
    }

    @GetMapping("add")
    public String add(){
        return PATH + "add";
    }

    @ResponseBody
    @PostMapping("add")
    public String doAdd(SampleCustomer sampleCustomer, RenderingVO renderingVO, String rel){
        List<CustomerRendering> renderings = renderingVO.getRendering().stream().filter(rendering -> rendering.getPreview() != null)
                .collect(Collectors.toList());
        sampleCustomer.setCustomerNo(CUSTOMER_NO_PREFIX + identifierService.build());
        sampleCustomer.setIsTop(0);
        sampleCustomerService.add(sampleCustomer, renderings);
        return DwzSupport.ajaxSuccess(DWZ.ADD_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @GetMapping("edit")
    public String edit(SampleCustomer sampleCustomer, Model model){
        sampleCustomer = sampleCustomerService.getById(sampleCustomer.getId());
        List<CustomerRendering> renderingList = customerRenderingService.selectListById(sampleCustomer.getId());
        model.addAttribute("sampleCustomer", sampleCustomer);
        model.addAttribute("renderingList", renderingList);
        return PATH + "edit";
    }

    @ResponseBody
    @PostMapping("edit")
    public String doEdit(SampleCustomer sampleCustomer, RenderingVO renderingVO, String rel){
        List<CustomerRendering> renderings = renderingVO.getRendering().stream().filter(rendering -> rendering.getPreview() != null)
                .collect(Collectors.toList());
        sampleCustomerService.update(sampleCustomer, renderings);
        return DwzSupport.ajaxSuccess(DWZ.EDIT_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @ResponseBody
    @PostMapping("top")
    public String top(SampleCustomer sampleCustomer, String rel){
        sampleCustomer.setIsTop(1);
        sampleCustomerService.update(sampleCustomer);
        return DwzSupport.ajaxSuccess(DWZ.OPR_SUCCESS, rel);
    }

    @ResponseBody
    @PostMapping("offTop")
    public String offTop(SampleCustomer sampleCustomer, String rel){
        sampleCustomer.setIsTop(0);
        sampleCustomerService.update(sampleCustomer);
        return DwzSupport.ajaxSuccess(DWZ.OPR_SUCCESS, rel);
    }


}
