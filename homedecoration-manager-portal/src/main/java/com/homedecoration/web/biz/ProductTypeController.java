package com.homedecoration.web.biz;

import com.biloba.common.domain.Expression;
import com.biloba.common.domain.Operation;
import com.biloba.common.domain.enumeration.AvailableEnum;
import com.biloba.common.domain.pojo.DomainPage;
import com.biloba.common.portal.support.DWZ;
import com.biloba.common.portal.support.DwzSupport;
import com.google.common.base.Strings;
import com.homedecoration.persist.domain.biz.ProductType;
import com.homedecoration.service.biz.ProductTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xy on 2018/9/30.
 */
@Controller
@RequestMapping("biz/productType")
public class ProductTypeController {
    private static final String PATH = "biz/productType/";

    @InitBinder("productType")
    private void initProductTypeForBinder(WebDataBinder binder){
        binder.setFieldDefaultPrefix("productType.");
    }

    @Resource
    private ProductTypeService productTypeService;

    @RequestMapping("search")
    public String search(ProductType productType, Model model, Integer pageIndex, Integer pageSize){
        List<Expression> expressionList = new ArrayList<>();
        if (!Strings.isNullOrEmpty(productType.getName())) {
            expressionList.add(new Expression("name", Operation.ALL_LIKE, productType.getName()));
        }
        DomainPage domainPage = productTypeService.selectByPage(expressionList, pageIndex, pageSize);
        model.addAttribute("domainPage", domainPage);
        model.addAttribute("productType", productType);
        return PATH + "list";
    }

    @GetMapping("add")
    public String add(){
        return PATH + "add";
    }

    @ResponseBody
    @PostMapping("add")
    public String doAdd(ProductType productType, String rel){
        productTypeService.add(productType);
        return DwzSupport.ajaxSuccess(DWZ.ADD_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @GetMapping("edit")
    public String edit(ProductType productType, Model model){
        productType = productTypeService.getById(productType.getId());
        model.addAttribute("productType", productType);
        return PATH + "edit";
    }

    @ResponseBody
    @PostMapping("edit")
    public String doEdit(ProductType productType, String rel){
        productTypeService.update(productType);
        return DwzSupport.ajaxSuccess(DWZ.EDIT_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @ResponseBody
    @PostMapping("delete")
    public String delete(ProductType productType, String rel){
        productType.setAvailable(AvailableEnum.UNAVAILABLE);
        productTypeService.update(productType);
        return DwzSupport.ajaxSuccess(DWZ.OPR_SUCCESS, rel);
    }
}
