package com.homedecoration.web.biz;

import com.biloba.common.core.constant.ConfigurationConst;
import com.biloba.common.core.constant.SystemConst;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.Operation;
import com.biloba.common.domain.pojo.DomainPage;
import com.biloba.common.portal.support.DWZ;
import com.biloba.common.portal.support.DwzSupport;
import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.homedecoration.common.util.HtmlUtil;
import com.homedecoration.persist.domain.biz.Product;
import com.homedecoration.persist.domain.biz.ProductType;
import com.homedecoration.service.biz.IdentifierService;
import com.homedecoration.service.biz.ProductService;
import com.homedecoration.service.biz.ProductTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * Created by xy on 2018/9/28.
 */
@Controller
@RequestMapping("biz/product")
public class ProductController {

    private static final String PATH = "biz/product/";

    public static final int PRODUCT_NO_PREFIX = 80;

    @InitBinder("product")
    private void initBinderForProduct(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("product.");
    }

    @Resource
    private ProductService productService;

    @Resource
    private ProductTypeService productTypeService;

    @Resource
    private IdentifierService identifierService;

    @RequestMapping("search")
    public String search(Model model, Integer pageIndex, Integer pageSize, Product product) {
        List<Expression> expressionList = Lists.newArrayList();
        if (!Strings.isNullOrEmpty(product.getProductName())) {
            expressionList.add(new Expression("productName", Operation.ALL_LIKE, product.getProductName()));
        }
        if (!Strings.isNullOrEmpty(product.getProductNo())) {
            expressionList.add(new Expression("productNo", Operation.ALL_LIKE, product.getProductNo()));
        }
        if (Objects.nonNull(product.getTypeId())) {
            expressionList.add(new Expression("typeId", Operation.EQUALS, product.getTypeId()));
        }
        DomainPage domainPage = productService.selectByPage(expressionList, pageIndex, pageSize);
        List<ProductType> productTypeList = productTypeService.selectListByExpressions(new ArrayList<>());
        List<Map<String, Object>> result =  parseData.apply(domainPage.getDomains());
        domainPage = new DomainPage(pageIndex, pageSize);
        domainPage.setDomains(result);
        model.addAttribute("domainPage", domainPage);
        model.addAttribute("product", product);
        model.addAttribute("productTypeList", productTypeList);
        return PATH + "list";
    }

    @GetMapping("add")
    public String add(Model model){
        List<ProductType> productTypeList = productTypeService.selectListByExpressions(new ArrayList<>());
        model.addAttribute("productTypeList", productTypeList);
        return PATH + "add";
    }

    @ResponseBody
    @PostMapping("add")
    public String doAdd(Product product, String addProductContent, String rel) throws IOException {
        product.setProductNo(PRODUCT_NO_PREFIX + identifierService.build());
        product.setHtmlUrl(HtmlUtil.uploadHtml(ConfigurationConst.RESOURCE_ROOT_PATH + "/template.html", addProductContent, null));
        product.setIsTop(0);
        productService.add(product);
        return DwzSupport.ajaxSuccess(DWZ.ADD_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @GetMapping("edit")
    public String edit(Product product, Model model){
        product = productService.getById(product.getId());
        List<ProductType> productTypeList = productTypeService.selectListByExpressions(new ArrayList<>());
        List<String> imageList = Splitter.on(SystemConst.SEPARATOR_SYMBOL).omitEmptyStrings().trimResults().splitToList(product.getImages());
        String content = HtmlUtil.parseHtml(ConfigurationConst.RESOURCE_ROOT_PATH + product.getHtmlUrl());
        model.addAttribute("product", product);
        model.addAttribute("productTypeList", productTypeList);
        model.addAttribute("imageList", imageList);
        model.addAttribute("content", content);
        return PATH + "edit";
    }

    @ResponseBody
    @PostMapping("edit")
    public String doEdit(Product product, String editProductContent, String rel) throws IOException {
        product.setHtmlUrl(HtmlUtil.uploadHtml(ConfigurationConst.RESOURCE_ROOT_PATH + "/template.html", editProductContent, null));
        productService.update(product   );
        return DwzSupport.ajaxSuccess(DWZ.EDIT_SUCCESS, rel, null, DWZ.CLOSE_CURRENT);
    }

    @ResponseBody
    @PostMapping("top")
    public String top(Product product, String rel){
        product.setIsTop(1);
        productService.update(product);
        return DwzSupport.ajaxSuccess(DWZ.OPR_SUCCESS, rel);
    }

    @ResponseBody
    @PostMapping("offTop")
    public String offTop(Product product, String rel){
        product.setIsTop(0);
        productService.update(product);
        return DwzSupport.ajaxSuccess(DWZ.OPR_SUCCESS, rel);
    }

    private Function<List<Product>, List<Map<String, Object>>> parseData = new Function<List<Product>, List<Map<String, Object>>>() {
        @Nullable
        @Override
        public List<Map<String, Object>> apply(@Nullable List<Product> productList) {
            List<Map<String, Object>> list = new ArrayList<>(productList.size());
            Map<String, Object> map;
            ProductType productType;
            for (Product product : productList) {
                map = new HashMap<>();
                productType = productTypeService.getById(product.getTypeId());
                map.put("product", product);
                map.put("productType", productType);
                list.add(map);
            }
            return list;
        }
    };

}
