/**
 * Copyright  2016  Pemass
 * All Right Reserved.
 */
package com.homedecoration.web;

import com.biloba.common.core.constant.ConfigurationConst;
import com.biloba.common.core.util.FileUtil;
import com.biloba.common.core.util.JacksonUtil;
import com.biloba.common.portal.support.Uploader;
import com.google.common.collect.Maps;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Description: ResourcesController
 * @Author: estn.zuo
 * @CreateTime: 2016-03-21 20:56
 */
@Controller
@RequestMapping("/upload")
public class UploadController {

    @RequestMapping()
    @ResponseBody
    public String insert(HttpServletRequest request) throws IOException {
        Map<String, Object> json = Maps.newHashMap();
        String relativeUrl = doUpload(request);
        json.put("url", relativeUrl);

        return JacksonUtil.write(json);
    }

    private String doUpload(HttpServletRequest request) {
        String relativeUrl = "";
        try {
            request.setCharacterEncoding("UTF-8");
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

            MultipartFile multipartFile = null;
            Map map = multipartRequest.getFileMap();
            for (Iterator i = map.keySet().iterator(); i.hasNext(); ) {
                Object obj = i.next();
                multipartFile = (MultipartFile) map.get(obj);
            }
            InputStream inputStream = multipartFile.getInputStream();
            File file = new File("");
            file.getAbsolutePath();
            relativeUrl = FileUtil.saveFile(inputStream, FileUtil.fetchExtension(multipartFile.getOriginalFilename()));
//            File f = new File(ConfigurationConst.RESOURCE_ROOT_PATH + relativeUrl);
//            if (f.exists() ) {
//                long size = f.length();
//                String reg ="(?i).+?\\.(jpg|gif|bmp|png)";
//                if (size > 1 * 1024 * 1024 && relativeUrl.matches(reg)) {
//                    Thumbnails.of(ConfigurationConst.RESOURCE_ROOT_PATH+relativeUrl).size(640,400).toFile(ConfigurationConst.RESOURCE_ROOT_PATH+relativeUrl);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return relativeUrl;
    }

    @RequestMapping("ueditor")
    public void uedit(HttpServletRequest request, HttpServletResponse response) throws Exception {

        request.setCharacterEncoding(Uploader.ENCODEING);
        response.setCharacterEncoding(Uploader.ENCODEING);

        String currentPath = request.getRequestURI().replace(request.getContextPath(), "");

        File currentFile = new File(currentPath);

        List<String> savePath = Arrays.asList("upload,upload2,upload3".split(","));


        //获取存储目录结构
        if (request.getParameter("fetch") != null) {

            response.setHeader("Content-Type", "text/javascript");

            //构造json数据
            Iterator<String> iterator = savePath.iterator();

            String dirs = "[";
            while (iterator.hasNext()) {

                dirs += "'" + iterator.next() + "'";

                if (iterator.hasNext()) {
                    dirs += ",";
                }

            }
            dirs += "]";
            response.getWriter().print("updateSavePath( " + dirs + " );");
            return;

        }

        Uploader up = new Uploader(request);

        // 获取前端提交的path路径
        String dir = request.getParameter("dir");


        //普通请求中拿不到参数， 则从上传表单中拿
        if (dir == null) {
            dir = up.getParameter("dir");
        }

        if (dir == null || "".equals(dir)) {

            //赋予默认值
            dir = savePath.get(0);

            //安全验证
        } else if (!savePath.contains(dir)) {

            response.getWriter().print("{'state':'\\u975e\\u6cd5\\u4e0a\\u4f20\\u76ee\\u5f55'}");
            return;

        }

        up.setSavePath(dir);
        String[] fileType = {".gif", ".png", ".jpg", ".jpeg", ".bmp"};
        up.setAllowFiles(fileType);
        up.setMaxSize(500 * 1024); //单位KB
        up.upload();
        File f = new File(ConfigurationConst.RESOURCE_ROOT_PATH + up.getRelativeURL());
        if (f.exists() ) {
            long size = f.length();
            if (size > 3 * 1024 * 1024) {
                Thumbnails.of(ConfigurationConst.RESOURCE_ROOT_PATH+up.getRelativeURL()).size(640,400).toFile(ConfigurationConst.RESOURCE_ROOT_PATH+up.getRelativeURL());
            }
        }
        response.getWriter().print("{'original':'" + up.getOriginalName() + "','url':'" + up.getUrl() + "','title':'" + up.getTitle() + "','state':'" + up.getState() + "'}");
    }
}

