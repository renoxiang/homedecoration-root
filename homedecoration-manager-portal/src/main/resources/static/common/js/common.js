$(function () {
    $(".logo").ifixpng();
    if ($.validator) {
        $.validator
            .addMethod(
            "phone",
            function (value, element) {
                return this.optional(element)
                    || /((1(([35][0-9])|(47)|[8][01236789]))\d{8})|(0\d{2,3}(\-)?\d{7,8})$/
                        .test(value);
            }, "请输入正确的电话号码!");
        $.validator.addClassRules({
            phone: {
                phone: true
            }
        });
    }
});

var doit = {
    preUploadSuccess: function (file, data, response) {
        if (!response) {
            return;
        }
        var $this = $(this);
        data = eval("(" + data + ")");
        $this.each(function (i) {
            var $wrapper = $this[i].wrapper;
            $wrapper.siblings("input[name='content.previewImage']").val(data.url);
            $wrapper.siblings("input[name='category.previewImage']").val(data.url);
            $wrapper.siblings("input[name='product.previewImage']").val(data.url);
            $wrapper.siblings("input[name='merchant.previewImage']").val(data.url);
            $wrapper.siblings("input[name='previewImage']").val(data.url);
            $wrapper.siblings("input[name='thumbnail']").val(data.url);

            var fileQue = $wrapper.siblings("div.thumbImg");
            fileQue.empty();
            var img = $("<img src='" + resourceRootUrl + data.url + "' height='150px'/>");
            fileQue.append(img);
        });
    },
    detailUploadSuccess: function (file, data, response) {
        if (!response) {
            return;
        }
        var $this = $(this);
        data = eval("(" + data + ")");
        $this.each(function (i) {
            var $wrapper = $this[i].wrapper;
            $wrapper.siblings("input#imgUrl").val(data.url);
            var fileQue = $wrapper.siblings("div.thumbImg");
            fileQue.empty();
            var img = $("<img src='" + resourceRootUrl + data.url + "' height='150px'/>");
            fileQue.append(img);
        });
    },
    videoUploadSuccess: function (file, data, response) {
        if (!response) {
            return;
        }
        var $this = $(this);
        data = eval("(" + data + ")");
        $this.each(function (i) {
            var $wrapper = $this[i].wrapper;
            $wrapper.siblings("input#videoUrl").val(data.videoUrl);
        });
    },
    attachmentUploadSuccess: function (file, data, response) {
        if (!response) {
            return;
        }
        var $this = $(this);


        console.info(data);
        alert(123);

        data = eval("(" + data + ")");
        $this.each(function (i) {
            var $wrapper = $this[i].wrapper;
            $wrapper.siblings("input[name='announcement.attachment']").val(data.url);
            var fileQue = $wrapper.siblings("div.attachment");
            fileQue.empty();
            var a = $(" <a href=" + resourceRootUrl + data.url + " >附件</a>");

            fileQue.append(a);
        });
    },
    root: function () {
        var curWwwPath = window.document.location.href;
        var pathName = window.document.location.pathname;
        var pos = curWwwPath.indexOf(pathName);
        var localhostPaht = curWwwPath.substring(0, pos);
        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
        return (localhostPaht + projectName);
    }
};
/**
 * 只能输入数字，如果为空，返回0
 */
function onlyNum(obj) {
    var str = obj.value;
    str = str.replace(/[^\d]/g, '');
    if (str == "")
        str = 0;
    if (str.length > 1 && str.charAt(0) == '0')
        str = str.substr(1);
    obj.value = str;
}

Array.prototype.max = function () { // 最大值
    return Math.max.apply({}, this);
};