/**
 * Created by xy on 2017/7/12.
 */
var maxPage;
(function ($) {
    $.ajax({
        type: "post",
        url: "http://182.150.2.204:8888/env-cd-manager-portal/biz/dynamic/dynamic_list",
        data:{"pageIndex":1,"pageSize":10},
        dataType: 'jsonp',
        jsonp: "callbackparam",
        success: function (result) {
            maxPage = result.maxPage;
            var $dynamic = $("#f_inews_lis_ul");
            for(var i=0;i<result.list.length;i++){
                var unixTimestamp = new Date(result.list[i].issueTime) ;
                var commonTime = unixTimestamp.Format("yyyy-MM-dd");;
                var content = " <li onclick='detail(this)'>" +
                    "<input type='hidden' value='http://182.150.2.204:8081/file"+ result.list[i].htmlUrl +"'/>"+
                    "<a class='f_inewsimg' href='javascript:void(0)'><img\n" +
                    "src='http://182.150.2.204:8081/file"+ result.list[i].previewImage +"'/></a>\n" +
                    "<div class='f_inewscon'>\n" +
                    "<h4><a href='javascript:void(0)'>"+result.list[i].title+"</a></h4>\n" +
                    "<p class='clears'><span>"+commonTime+"</span></p>\n" +
                    "</div>\n" +
                    "</li>";
                $dynamic.append(content);
            }
            if(maxPage == 1){
                $("#div_getmorenews").hide()
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
})(jQuery)
function detail(obj){
    var url = $(obj).children('input').val();
    window.location.href=url;
}
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
var indexPage = 1;
var dynamic = {

    loadMore: function(){
    ++indexPage;
    if(indexPage == maxPage){
        $("#div_getmorenews").hide()
    }
    $.ajax({
        type: "post",
        url: "http://182.150.2.204:8888/env-cd-manager-portal/biz/dynamic/dynamic_list",
        data:{"pageIndex":indexPage,"pageSize":10},
        dataType: 'jsonp',
        jsonp: "callbackparam",
        success: function (result) {
            var $dynamic = $("#f_inews_lis_ul");
            for(var i=0;i<result.list.length;i++){
                var unixTimestamp = new Date(result.list[i].issueTime) ;
                var commonTime = unixTimestamp.toLocaleString();
                var content = " <li onclick='detail(this)'>" +
                    "<input type='hidden' value='http://182.150.2.204:8081/file"+ result.list[i].htmlUrl +"'/>"+
                    "<a class='f_inewsimg' href='javascript:void(0)'><img\n" +
                    "src='http://182.150.2.204:8081/file"+ result.list[i].previewImage +"'/></a>\n" +
                    "<div class='f_inewscon'>\n" +
                    "<h4><a href='javascript:void(0)'>"+result.list[i].title+"</a></h4>\n" +
                    "<p class='clears'><span>"+commonTime+"</span></p>\n" +
                    "</div>\n" +
                    "</li>";
                $dynamic.append(content)
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}
}
