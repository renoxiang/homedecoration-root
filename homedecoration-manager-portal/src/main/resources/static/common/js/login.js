$(function() {
	// 解决IE6 PNG不透明
	$("img").ifixpng();
	var $captchaImage = $("#captchaImage");
	$("#subForm").validate({
		focusInvalid : false,
		focusCleanup : true,
		errorElement : "span",
		ignore : ".ignore",
		submitHandler : function(form) {
			form.submit();
		},
		rules : {
			captcha : {
				required : true
			},
			password : {
				required : true
			},
			username : {
				required : true
			}
		},
		messages : {
			username : {
				required : "请输入用户名!"
			},
			password : {
				required : "请输入密码!"
			},
			captcha : {
				required : "请输入验证码!",
                maxlength:"请输入4个字符的验证码"
			}
		}
	});

	$captchaImage.siblings("a").click(function() {
		var timestamp = (new Date()).valueOf();
		var imageSrc = $captchaImage.attr("src");
		if (imageSrc.indexOf("?") >= 0) {
			imageSrc = imageSrc.substring(0, imageSrc.indexOf("?"));
		}
		imageSrc = imageSrc + "?timestamp=" + timestamp;
		$captchaImage.attr("src", imageSrc);
	});

});