var pemass = {
    //配置from信息
    createFrom: function (actionName) {
        var fromFile = document.createElement("FORM");
        document.body.appendChild(fromFile);
        fromFile.method = "POST";
        fromFile.action = actionName;
        fromFile.style.display = "none";
        return fromFile;
    },
    createGetFrom: function (actionName) {
        var fromFile = document.createElement("FORM");
        document.body.appendChild(fromFile);
        fromFile.method = "GET";
        fromFile.action = actionName;
        fromFile.style.display = "none";
        return fromFile;
    },
    //配置from信息1
    createFrom1: function (actionName) {
        var fromFile = document.createElement("FORM");
        document.body.appendChild(fromFile);
        fromFile.method = "POST";
        fromFile.encding = "multipart/form-data";
        fromFile.action = actionName;
        fromFile.style.display = "none";
        return fromFile;
    },
    //配置from信息
    createParentFrom: function (actionName) {
        var fromFile = window.parent.document.createElement("FORM");
        window.parent.document.body.appendChild(fromFile);
        fromFile.method = "POST";
        fromFile.action = actionName;
        fromFile.style.display = "none";
        return fromFile;
    },
//注入到from数据中
    loadFrom: function (name, value, fromFile) {
        var input = document.createElement("INPUT");
        input.type = "hidden";
        input.name = name;
        input.id = name;
        input.value = value;
        fromFile.appendChild(input);
    },

    loadFromInput: function (name, value, fromFile) {
        var input = document.createElement("INPUT");
        input.type = "hidden";
        input.name = name;
        input.value = value;
        fromFile.appendChild(input);
    },
    loadParentFrom: function (name, value, fromFile) {
        var input = window.parent.document.createElement("INPUT");
        input.type = "hidden";
        input.name = name;
        input.id = name;
        input.value = value;
        fromFile.appendChild(input);
    },
    //注入到from数据中
    loadFromToname: function (name, value, fromFile) {
        var input = document.createElement("INPUT");
        input.type = "hidden";
        input.name = name;
        input.value = value;
        fromFile.appendChild(input);
    },
    getBrowserVersion: function () {
        var browser = {};
        var userAgent = navigator.userAgent.toLowerCase();
        var s;
        (s = userAgent.match(/msie ([\d.]+)/))
            ? browser.ie = s[1]
            : (s = userAgent.match(/firefox\/([\d.]+)/))
            ? browser.firefox = s[1]
            : (s = userAgent.match(/chrome\/([\d.]+)/))
            ? browser.chrome = s[1]
            : (s = userAgent.match(/opera.([\d.]+)/))
            ? browser.opera = s[1]
            : (s = userAgent
            .match(/version\/([\d.]+).*safari/))
            ? browser.safari = s[1]
            : 0;
        var version = "";
        if (browser.ie) {
            version = "msie " + browser.ie;
        } else if (browser.firefox) {
            version = "firefox " + browser.firefox;
        } else if (browser.chrome) {
            version = "chrome " + browser.chrome;
        } else if (browser.opera) {
            version = "opera " + browser.opera;
        } else if (browser.safari) {
            version = "safari " + browser.safari;
        } else {
            version = "未知浏览器";
        }
        return version;
    },
    /**
     * 返回当前项目的路径包括(IP、端口、Context)，主要用于在js代码
     * @return
     */
    root: function () {
        var curWwwPath = window.document.location.href;
        var pathName = window.document.location.pathname;
        var pos = curWwwPath.indexOf(pathName);
        var localhostPath = curWwwPath.substring(0, pos);
        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
        return (localhostPath + projectName);
    },

    emptySelectValue: '$("<option value=\'\'>==请选择==</option>)")',

    /**
     * 初始化省市区
     * @param province
     * @param city
     * @param district
     */
    init: function (province, city, district) {
        /**转换为jQuery对象*/
        province = "#" + province;
        city = "#" + city;
        district = "#" + district;

        var $province = $(province);
        var $city = $(city);
        var $district = $(district);

        var cityId = $city.val();
        var provinceId = $province.val();
        var districtId = $district.val();
        $.getJSON("../pemass/json/province-city-district.json", function (data) {
            $.each(data, function (i, province) {
                if (province.pn != '') {
                    if (provinceId == province.id) {
                        $("#province").append("<option value='" + province.id + "' selected>" + province.pn + "</option>");
                        $.each(province.cpl, function (j, city) {
                            if (city.cn != '') {
                                if (city.id == cityId) {
                                    $("#city").append("<option value='" + city.id + "' selected>" + city.cn + "</option>");
                                    $.each(city.dpl, function (k, district) {
                                        if (district.cn != '') {
                                            if (district.id == districtId) {
                                                $("#district").append("<option value='" + district.id + "' selected>" + district.dn + "</option>");
                                            } else {
                                                $("#district").append("<option value='" + district.id + "'>" + district.dn + "</option>");
                                            }
                                        }
                                    })
                                } else {
                                    $("#city").append("<option value='" + city.id + "'>" + city.cn + "</option>");
                                }
                            }
                        })
                    } else {
                        $("#province").append("<option value='" + province.id + "'>" + province.pn + "</option>");
                    }
                }
            })
        });
    },
    /**
     * 省市区下拉列表
     *
     * @param province  省select标签的id名称
     * @param city  市select标签的id名称
     * @param district  区select标签的id名称
     */
    changeProvince: function (province, city, district) {

        /**转换为jQuery对象*/
        province = "#" + province;
        city = "#" + city;
        district = "#" + district;

        var $province = $(province);
        var $city = $(city);
        var $district = $(district);

        /**清空市和区,只给出市区一个默认值*/
        $city.empty().append(pemass.emptySelectValue);
        $district.empty().append(pemass.emptySelectValue);


        /**获取数据*/
        var provinceId = $province.val();
        $.getJSON("../pemass/json/province-city-district.json", function (data) {
            $.each(data, function (i, province) {
                if (province.pn != '' && province.id == provinceId) {
                    $.each(province.cpl, function (j, city) {
                        var $option = $("<option value='" + city.id + "'>" + city.cn + "</option>");
                        $city.append($option);
                    })
                }
            })
        });
    },

    changeCity: function (province, city, district) {
        province = "#" + province;
        city = "#" + city;
        district = "#" + district;

        var $province = $(province);
        var $city = $(city);
        var $district = $(district);
        $district.empty().append(pemass.emptySelectValue);

        var cityId = $city.val();
        var provinceId = $province.val();
        $.getJSON("../pemass/json/province-city-district.json", function (data) {
            $.each(data, function (i, province) {
                if (province.pn != '' && province.id == provinceId) {
                    $.each(province.cpl, function (j, city) {
                        if (city.cn != '' && city.id == cityId) {
                            $("#district").empty();
                            $("#district").append("<option value=''>==请选择==</option>");
                            $.each(city.dpl, function (k, district) {
                                $("#district").append("<option value='" + district.id + "'>" + district.dn + "</option>");
                            })
                        }
                    })
                }
            })
        });
    },

    /**
     * 只能输入数字，如果为空，返回0
     * 在onkeyup事件中调用此方法
     * <p>
     * <input type="text"  onkeydown="pemass.checkNumber(event);">
     */
    checkNumber: function (event) {
        if ((event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 8 || (event.keyCode >= 96 && event.keyCode <= 105)
            || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 190) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    },
    /**
     * 只能输入整数，如果为空，返回0
     * 在onkeyup事件中调用此方法
     * <p>
     * <input type="text"  onkeydown="pemass.checkInteger(event);">
     */
    checkInteger: function (event) {
        if (!(event.keyCode == 46) && !(event.keyCode == 8) && !(event.keyCode == 37) && !(event.keyCode == 39))
            if (!((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)))
                event.returnValue = false;
    },
    popupGritterMessage: function (popup, title, text, className, time) {
        if (popup) {
            var $gritterMessage = $("<input type='hidden' id='gritterMessage'>");
            $gritterMessage.on(ace.click_event, function () {
                $.gritter.add({
                    title: title,
                    text: text,
                    class_name: className,
                    time: time
                });
                return true;
            }).trigger("click");
        }
    },
    initializeDate: function (dateId) {
        $("#" + dateId).datepicker({
            showOtherMonths: true,
            selectOtherMonths: false,
            dateFormat: 'yy-mm-dd',
            dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        });
    },
    /**
     * 四舍五入法截取一个小数
     * @param float digit 要格式化的数字
     * @param integer length 要保留的小数位数
     * @return float
     */
    digitRound: function (digit, length) {
        length = length ? parseInt(length) : 0;
        if (length <= 0) return Math.round(digit);
        digit = Math.round(digit * Math.pow(10, length)) / Math.pow(10, length);
        return digit;
    },
    /**
     * 进一法截取一个小数
     * @param float digit 要格式化的数字
     * @param integer length 要保留的小数位数
     * @return float
     */
    digitCeil: function (digit, length) {
        length = length ? parseInt(length) : 0;
        if (length <= 0) return Math.ceil(digit);
        digit = Math.ceil(digit * Math.pow(10, length)) / Math.pow(10, length);
        return digit;
    },
    getElements: function getElements(formId) {
        var form = document.getElementById(formId);
        var elements = new Array();
        var tagElements = form.getElementsByTagName('input');
        for (var j = 0; j < tagElements.length; j++) {
            elements.push(tagElements[j]);

        }
        return elements;
    },
    inputSelector: function inputSelector(element) {
        if (element.checked) {
            return [element.name, element.value];
        }
    },
    input: function input(element) {
        switch (element.type.toLowerCase()) {
            case 'submit':
            case 'hidden':
            case 'password':
            case 'text':
                return [element.name, element.value];
            case 'checkbox':
            case 'radio':
                return this.inputSelector(element);
        }
        return false;
    },
    serializeElement: function serializeElement(element) {
        var method = element.tagName.toLowerCase();
        var parameter = this.input(element);
        if (parameter) {
            var key = encodeURIComponent(parameter[0]);
            if (key.length == 0) return;

            if (parameter[1].constructor != Array)
                parameter[1] = [parameter[1]];
            var values = parameter[1];
            var results = [];
            for (var i = 0; i < values.length; i++) {
                results.push(key + '=' + values[i]);
            }
            return results.join('&');
        }
    },
    serializeForm: function serializeForm(formId) {
        var elements = this.getElements(formId);
        var queryComponents = new Array();

        for (var i = 0; i < elements.length; i++) {
            var queryComponent = this.serializeElement(elements[i]);
            if (queryComponent)
                queryComponents.push(queryComponent);
        }

        return queryComponents.join('&');
    },
    getParameterJsonString: function (formElement) {
        var parameterJsonString = '{';
        $(formElement).find("input,textarea,select").each(function () {
            // 如果为单选或者复选的INPUT,且该元素为为选中状态,该元素跳过
            if ($(this).prop("disabled") == true) {
                return true;
            }
            var isInput = $(this).is("input");
            if (isInput) {
                var inputType = $(this).prop("type");
                if (inputType == "radio" || inputType == "checkbox") {
                    var checkedStatus = $(this).prop("checked");
                    if (!checkedStatus) return true;  // 返回true跳出当前循环
                }
                if (inputType == "button") {
                    return true;
                }
            }

            var elementName = $(this).prop("name");
            elementName = $.trim(elementName);
            var elementValue = $(this).prop("value");
            elementValue = $.trim(elementValue);

            // 清除'"'和空白符
            var pattern = new RegExp("(\\s|\")+", "g");
            elementName = elementName.replace(pattern, '');
            elementValue = elementValue.replace(pattern, '');
            if (elementName.length > 0 && elementValue.length > 0) {
                parameterJsonString += '\"' + elementName + '\":\"' + elementValue + '\",';
            }

        });
        var strLength = parameterJsonString.length;
        parameterJsonString = parameterJsonString.substring(0, strLength - 1);
        parameterJsonString += '}';
        parameterJsonString = strEnc(parameterJsonString, "dff11af6e4e04c50", "owbthxvjmsrraqppvv", "abumnpilaupsxqsugu");

        return parameterJsonString;
    },
    /**
     *  handleType:{0 - 新增修改; 1 - 页面跳转; 2 - 搜索表单提交; 3 - 当前页面弹出(dialog, 例如审核操作)}
     **/
    newEncryptDate: function (formElement, handleType, confirmMsg) {
        /* 从表单中获取元素生成待加密字符串 */
        var parameterJsonString = pemass.getParameterJsonString(formElement);

        /* 将字符串进行加密处理 并提交表单 */
        $.ajax({
            type: "POST",
            url: pemass.root() + "/encrypt_parameter!encryptParameter.action",
            data: {
                "parameterJsonString": parameterJsonString
            },
            async: false,
            dataType: "json",
            success: function (data) {
                /* 处理返回数据 生成相应的URL */
                var url = strDec(data, "dff11af6e4e04c50", "owbthxvjmsrraqppvv", "abumnpilaupsxqsugu");
                var requestURL = $(formElement).prop("action");
                if (requestURL.indexOf("\?") != -1) requestURL = requestURL.substr(0, requestURL.indexOf("\?"));
                $(formElement).prop("action", requestURL + "?" + url);

                /* 操作完成后的表单提交操作 */
                switch (handleType) {
                    case 0:
                        validateCallback(formElement, navTabAjaxDone, confirmMsg);
                        return;
                    case 1:
                        return;
                    case 2:
                        navTabSearch(formElement);
                        return;
                    case 3:
                        validateCallback(formElement, dialogAjaxDone, confirmMsg);
                        return;
                    default :
                }
            }
        });
    },
    encryptData: function (formElement, isAjax, confirmMsg, isDialog) {
        /* 从表单中获取元素生成待加密字符串 */
        var parameterJsonString = pemass.getParameterJsonString(formElement);

        /* 将字符串进行加密处理 并提交表单 */
        $.ajax({
            type: "POST",
            url: pemass.root() + "/encrypt_parameter!encryptParameter.action",
            data: {
                "parameterJsonString": parameterJsonString
            },
            timeout:180000,
            async: false,
            dataType: "json",
            success: function (data) {
                var url = strDec(data, "dff11af6e4e04c50", "owbthxvjmsrraqppvv", "abumnpilaupsxqsugu");

                // 加密数据
                var requestURL = $(formElement).prop("action");
                if (requestURL.indexOf("\?") != -1) requestURL = requestURL.substr(0, requestURL.indexOf("\?"));
                $(formElement).prop("action", requestURL + "?" + url);
                if (isAjax) {
                    // 提交表单
                    if (!isDialog)
                        validateCallback(formElement, navTabAjaxDone, confirmMsg);
                    else
                        validateCallback(formElement, dialogAjaxDone, confirmMsg);
                } else {
                    formElement.submit();
                }
            }
        });
    },
    strEnc:function(parameterJsonString) {
        parameterJsonString = strEnc(parameterJsonString, "dff11af6e4e04c50", "owbthxvjmsrraqppvv", "abumnpilaupsxqsugu");
        return parameterJsonString;
    },
    strDec:function(parameterJsonString){
        var url =strDec(parameterJsonString,"dff11af6e4e04c50","owbthxvjmsrraqppvv","abumnpilaupsxqsugu");
        return url;
    }
};
