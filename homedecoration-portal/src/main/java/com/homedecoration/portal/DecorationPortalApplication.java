package com.homedecoration.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.homedecoration")
public class DecorationPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DecorationPortalApplication.class, args);
	}
}
