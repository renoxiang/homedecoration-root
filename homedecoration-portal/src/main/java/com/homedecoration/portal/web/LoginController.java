/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.homedecoration.portal.web;

import com.biloba.common.portal.support.DwzSupport;
import com.octo.captcha.CaptchaException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/login")
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @RequestMapping(method = RequestMethod.GET)
    public String login(HttpServletRequest request, HttpServletResponse response) {
        String xRequestedWith = request.getHeader("x-requested-with");
        if ("XMLHttpRequest".equals(xRequestedWith)) {
            try {
                response.setContentType("text/html" + ";charset=UTF-8");
                response.setHeader("Connection", "keep-alive");
                response.setDateHeader("Expires", 0);
                response.getWriter().write(DwzSupport.ajaxLogout("会话超时请重新登录"));
                response.getWriter().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        return "login";
    }


    @RequestMapping(method = RequestMethod.POST)
    public String login(HttpServletRequest request, RedirectAttributes rediect) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UsernamePasswordToken upt = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(upt);
        } catch (DisabledAccountException e) {
            e.printStackTrace();
            rediect.addFlashAttribute("errorText", "账号已冻结!");
            return "redirect:/login";
        } catch (UnknownAccountException e){
            e.printStackTrace();
            rediect.addFlashAttribute("errorText", "账号已删除!");
            return "redirect:/login";
        } catch (AuthenticationException e) {
            e.printStackTrace();
            rediect.addFlashAttribute("errorText", "您的账号或密码输入错误!");
            return "redirect:/login";
        } catch (CaptchaException e) {
            e.printStackTrace();
            rediect.addFlashAttribute("errorText", "验证码错误!");
            return "redirect:/login";
        }

        return "redirect:/";
    }



}

