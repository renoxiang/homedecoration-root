/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.homedecoration.portal.common.constant;

/**
 * @Description: ApiConst
 * @Author: estn.zuo
 * @CreateTime: 2017-03-15 10:31
 */
public class PortalConst {

    private PortalConst(){

    }

    /**
     * 超级管理员
     */
    public static final String ADMIN = "admin";
    /**
     * 预审员
     */
    public static final String PRETRIAL = "pretrial";

    /**
     * 部门回复员
     */
    public static final String DEPARTMENT_REPLY = "department_reply";

    /**
     * 区县回复员
     */
    public static final String DISTRICT_REPLY = "district_reply";

    /**
     * 投诉发布员
     */
    public static final String COMPLAINT_RELEASE = "complaint_release";

    /**
     * 动态发布员
     */
    public static final String DYNAMIC_RELEASE = "dynamic_release";
}
