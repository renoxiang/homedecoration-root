package com.homedecoration.service.sys.impl;

import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.Operation;
import com.biloba.common.domain.enumeration.AvailableEnum;
import com.biloba.common.domain.pojo.DomainPage;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.homedecoration.persist.domain.sys.Token;
import com.homedecoration.persist.domain.sys.User;
import com.homedecoration.persist.vo.UserVO;
import com.homedecoration.service.bas.TokenService;
import com.homedecoration.service.sys.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: ResourcesServiceImpl
 * @Author: pokl
 * @CreateTime: 2017-06-21 09:45
 */
@Service
public class UserServiceImpl implements UserService {

    private Log logger = LogFactory.getLog(UserServiceImpl.class);

    @Resource
    private BaseDao jpaBaseDao;

    @Resource
    private TokenService tokenService;


    @Transactional
    @Override
    public Token login(String username, String validateCode, Long deviceId) {
        logger.debug("验证验证码");
        /**测试账号跳过检验**/



        logger.debug("获取用户信息");
        User user = jpaBaseDao.getEntityByField(User.class, "username", username);

        logger.debug("获取设备信息");



        Token resultToken = new Token();
        /**用户名是否存在 不存在则创建账号*/
        if (user == null) {
            user = new User();
            user.setUsername(username);

            jpaBaseDao.persist(user);

            resultToken.setDeviceId(deviceId);
            resultToken.setTargetId(user.getId());
            resultToken.setTargetUUID(user.getUuid());
            tokenService.insert(resultToken);

            return resultToken;
        }
        resultToken = tokenService.retrieveTokenByUid(user.getId());
        resultToken.setAvailable(AvailableEnum.UNAVAILABLE);
        jpaBaseDao.merge(resultToken);

        Token newToken = new Token();
        newToken.setTargetUUID(user.getUuid());
        newToken.setTargetId(user.getId());
        tokenService.insert(newToken);

        return newToken;

    }

    @Override
    public User getByUsername(String username) {
        return jpaBaseDao.getEntityByField(User.class,"username",username);
    }

    @Transactional
    @Override
    public void update(User user) {
        User storeUser = jpaBaseDao.getEntityById(User.class, user.getId());

        BeanUtil.merge(user, storeUser);
        jpaBaseDao.merge(storeUser);
    }

    @Override
    public User getById(Long uid) {
        return jpaBaseDao.getEntityById(User.class,uid);
    }

    @Override
    public DomainPage selectUsersByPage(Map<String, Object> map, Integer pageIndex, Integer pageSize) {
        Optional<Object> username = Optional.fromNullable(map.get("username"));
        Optional<Object> startTime = Optional.fromNullable(map.get("startTime"));
        Optional<Object> endTime = Optional.fromNullable(map.get("endTime"));
        List<Expression> expressionList = Lists.newArrayList();
        if (username.isPresent()) {
            Expression expression = new Expression("username", Operation.ALL_LIKE, username.get());
            expressionList.add(expression);
        }
        if (startTime.isPresent()) {
            Expression expression = new Expression("createTime", Operation.GREATER_THAN_EQUALS,
                    new DateTime(startTime.get()).millisOfDay().withMinimumValue().toDate());
            expressionList.add(expression);
        }
        if (endTime.isPresent()) {
            Expression expression = new Expression("createTime", Operation.LESS_THAN_EQUALS,
                    new DateTime(endTime.get()).plusHours(23).plusMinutes(59).plusSeconds(59).toDate());
            expressionList.add(expression);
        }
        DomainPage domainPage = jpaBaseDao.getEntitiesPagesByExpressionList(User.class, expressionList, pageIndex, pageSize);
        domainPage.setDomains(this.getUserVOList.apply(domainPage.getDomains()));
        return domainPage;
    }

    @Override
    public List<User> selectUserList(String username) {
        List<Expression> expressionList = ImmutableList.of(
                new Expression("username", Operation.ALL_LIKE, username)
        );
        return jpaBaseDao.getEntitiesByExpressionList(User.class, expressionList);
    }

    private Function<List<User>, List<UserVO>> getUserVOList = new Function<List<User>, List<UserVO>>() {
        @Nullable
        @Override
        public List<UserVO> apply(@Nullable List<User> users) {
            List<UserVO> userVOList = new ArrayList<>(users.size());
            UserVO vo = null;
            for (User user : users) {
                vo = new UserVO();
                vo.setId(user.getId());
                vo.setUsername(user.getUsername());
                vo.setNikename(user.getNickname() == null ? "-" : user.getNickname());
                vo.setCreateTime(user.getCreateTime());
                userVOList.add(vo);
            }
            return userVOList;
        }
    };
}
