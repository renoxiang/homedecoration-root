package com.homedecoration.service.sys.impl;

import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.homedecoration.persist.domain.sys.Role;
import com.homedecoration.service.sys.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: yang
 * Date: 2017-06-06
 * Time: 10:13
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private BaseDao jpaBaseDao;

    @Override
    public List<Role> selectRoleList(List<Expression> expressionList) {
        return jpaBaseDao.getEntitiesByExpressionList(Role.class, expressionList);
    }

    @Override
    public Role selectRoleById(Long roleId) {
        return jpaBaseDao.getEntityById(Role.class, roleId);
    }
}
