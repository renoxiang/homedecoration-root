package com.homedecoration.service.sys;

import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.sys.Token;
import com.homedecoration.persist.domain.sys.User;

import java.util.List;
import java.util.Map;

/**
 * @Description: ResourcesServiceImpl
 * @Author: pokl
 * @CreateTime: 2017-06-21 09:45
 */
public interface UserService {


    Token login(String username, String validateCode, Long deviceId);

    User getByUsername(String username);

    void update(User user);

    User getById(Long uid);

    /**
     * 分页查询用户信息
     * @param map
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DomainPage selectUsersByPage(Map<String, Object> map, Integer pageIndex, Integer pageSize);

    /**
     * 通过用户名模糊匹配用户列表
     * @param username
     * @return
     */
    List<User> selectUserList(String username);
}
