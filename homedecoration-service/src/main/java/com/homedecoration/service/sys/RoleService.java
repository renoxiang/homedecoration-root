package com.homedecoration.service.sys;

import com.biloba.common.domain.Expression;
import com.homedecoration.persist.domain.sys.Role;

import java.util.List;

/**
 * User: yang
 * Date: 2017-06-06
 * Time: 10:13
 */
public interface RoleService {


    /**
     * 查询角色集合
     */
    List<Role> selectRoleList(List<Expression> expressionList);

    /**
     * 查询角色信息
     * @param roleId
     * @return
     */
    Role selectRoleById(Long roleId);
}
