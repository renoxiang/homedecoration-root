package com.homedecoration.service.sys.impl;


import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.core.util.encrypt.BCryptUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.homedecoration.persist.dao.sys.OperatorDao;
import com.homedecoration.persist.domain.sys.Operator;
import com.homedecoration.persist.domain.sys.Role;
import com.homedecoration.persist.vo.OperatorVO;
import com.homedecoration.service.sys.OperatorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * User: yang
 * Date: 2017-05-05
 * Time: 15:42
 */
@Service
public class OperatorServiceImpl implements OperatorService {

    @Resource
    private BaseDao jpaBaseDao;

    @Resource
    private OperatorDao operatorDao;

    @Override
    public com.homedecoration.persist.domain.sys.Operator selectByOperatorname(String operatorname) {
        Preconditions.checkNotNull(operatorname);
        return jpaBaseDao.getEntityByField(Operator.class, "operatorname", operatorname);
    }

    @Override
    public DomainPage selectOperatorsByPage(List<Expression> expressionList, Integer pageIndex, Integer pageSize) {
        Preconditions.checkNotNull(pageIndex);
        Preconditions.checkNotNull(pageSize);
        DomainPage domainPage = jpaBaseDao.getEntitiesPagesByExpressionList(Operator.class,
                expressionList, pageIndex, pageSize);
        domainPage.setDomains(this.getOperatorVOList.apply(domainPage.getDomains()));
        return domainPage;
    }

    @Override
    public Operator selectOperatorById(Long operatorId) {
        return jpaBaseDao.getEntityById(Operator.class, operatorId);
    }

    @Override
    public List<Operator> selectOperatorList(List<Expression> expressionList) {
        return jpaBaseDao.getEntitiesByExpressionList(Operator.class, expressionList);
    }

    @Transactional
    @Override
    public void insertOperator(Operator operator) {
        operator.setPassword(BCryptUtil.encrypt(operator.getPassword()));
        jpaBaseDao.persist(operator);
    }

    @Transactional
    @Override
    public void updateOperator(Operator operator) {
        if (operator.getPassword() != null) {
            operator.setPassword(BCryptUtil.encrypt(operator.getPassword()));
        }
        Operator target = jpaBaseDao.getEntityById(Operator.class, operator.getId());
        BeanUtil.merge(operator, target);
        jpaBaseDao.merge(target);
    }

    @Transactional
    @Override
    public void updateOperatorWithNull(Operator operator) {
        jpaBaseDao.merge(operator);
    }

    @Override
    public Boolean checkNameRepeat(String operatorName, Long operatorId) {
        Operator operator = this.selectByOperatorname(operatorName);
        if (operatorId == null) {
            return operator != null;
        }
        if (operator != null) {
            return !operator.getId().equals(operatorId);
        }
        return false;
    }

    @Override
    public Operator selectAllOperatorByName(String operatorname) {
        return operatorDao.selectAllOperatorByName(operatorname);
    }

    private Function<List<Operator>,List<OperatorVO>> getOperatorVOList = new Function<List<Operator>, List<OperatorVO>>() {
        @Nullable
        @Override
        public List<OperatorVO> apply(@Nullable List<Operator> operators) {
            List<OperatorVO> operatorVOList = new ArrayList<>(operators.size());
            OperatorVO vo;
            Role role;
            for(Operator operator : operators){
                role = jpaBaseDao.getEntityById(Role.class, operator.getRoleId());

                vo = new OperatorVO();
                vo.setId(operator.getId());
                vo.setNickname(operator.getNickname());
                vo.setPassword(operator.getPassword());
                vo.setOperatorname(operator.getOperatorname());
                vo.setTelephone(operator.getTelephone());
                vo.setRole(role.getRoleName());
                vo.setOperatorStatus(operator.getOperatorStatus());
                operatorVOList.add(vo);
            }
            return operatorVOList;
        }
    };
}
