package com.homedecoration.service.sys;


import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.sys.Operator;

import java.util.List;

/**
 * User: yang
 * Date: 2017-05-05
 * Time: 15:41
 */
public interface OperatorService {

    /**
     * 根据操作员账号查找信息
     * @param operatorname 操作员账号
     * @return
     */
   Operator selectByOperatorname(String operatorname);

    /**
     * 分页查询操作员数据
     * @param expressionList
     * @param pageIndex
     * @param pageSize
     * @return
     */
   DomainPage selectOperatorsByPage(List<Expression> expressionList, Integer pageIndex, Integer pageSize);

    /**
     * 根据id查询操作员信息
     * @param operatorId
     * @return
     */
   Operator selectOperatorById(Long operatorId);

    /**
     * 查询管理员集合
     * @return
     */
   List<Operator> selectOperatorList(List<Expression> expressionList);

    /**
     * 新增管理员信息
     * @param operator
     */
   void insertOperator(Operator operator);

    /**
     * 更新管理员信息
     * @param operator
     */
   void updateOperator(Operator operator);

    /**
     * 更新管理员信息（可以更新为Null的字段）
     * @param operator
     */
   void updateOperatorWithNull(Operator operator);

    /**
     * 检验账号是否重复
     * @param operatorName
     * @return 重复返回true
     */
    Boolean checkNameRepeat(String operatorName, Long operatorId);

    /**
     * 查询操作员（包括已删除的）
     * @param operatorname
     * @return
     */
    Operator selectAllOperatorByName(String operatorname);
}
