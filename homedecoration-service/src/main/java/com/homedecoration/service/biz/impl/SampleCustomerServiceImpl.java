package com.homedecoration.service.biz.impl;

import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.Operation;
import com.biloba.common.domain.enumeration.AvailableEnum;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.CustomerRendering;
import com.homedecoration.persist.domain.biz.SampleCustomer;
import com.homedecoration.service.biz.SampleCustomerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
@Service
public class SampleCustomerServiceImpl implements SampleCustomerService {

    @Resource
    private BaseDao jpaBaseDao;

    @Override
    public DomainPage selectByPage(List<Expression> expressions, Integer pageIndex, Integer pageSize) {
        return jpaBaseDao.getEntitiesPagesByExpressionList(SampleCustomer.class, expressions, pageIndex, pageSize);
    }

    @Transactional
    @Override
    public void add(SampleCustomer sampleCustomer, List<CustomerRendering> renderingList) {
        jpaBaseDao.persist(sampleCustomer);
        for (CustomerRendering rendering : renderingList) {
           rendering.setCustomerId(sampleCustomer.getId());
           jpaBaseDao.persist(rendering);
        }
    }

    @Transactional
    @Override
    public void update(SampleCustomer sampleCustomer, List<CustomerRendering> renderingList) {
        List<CustomerRendering> source = jpaBaseDao.getEntitiesByField(CustomerRendering.class, "customerId", sampleCustomer.getId());
        for (CustomerRendering rendering : source) {
            rendering.setAvailable(AvailableEnum.UNAVAILABLE);
            jpaBaseDao.merge(rendering);
        }

        update(sampleCustomer);

        for (CustomerRendering rendering : renderingList) {
            rendering.setCustomerId(sampleCustomer.getId());
            jpaBaseDao.persist(rendering);
        }
    }

    @Transactional
    @Override
    public void update(SampleCustomer sampleCustomer) {
        SampleCustomer target = getById(sampleCustomer.getId());
        BeanUtil.merge(sampleCustomer, target);
        jpaBaseDao.merge(target);
    }

    @Override
    public SampleCustomer getById(Long sampleCustomerId) {
        return jpaBaseDao.getEntityById(SampleCustomer.class, sampleCustomerId);
    }
}
