package com.homedecoration.service.biz;

/**
 * Created by xy on 2018/9/28.
 */
public interface IdentifierService
{
    String build();
}
