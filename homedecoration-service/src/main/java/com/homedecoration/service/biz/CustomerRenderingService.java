package com.homedecoration.service.biz;

import com.homedecoration.persist.domain.biz.CustomerRendering;

import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
public interface CustomerRenderingService {
    List<CustomerRendering> selectListById(Long customerId);
}
