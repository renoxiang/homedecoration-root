package com.homedecoration.service.biz.impl;

import com.homedecoration.service.biz.IdentifierService;
import org.joda.time.DateTime;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by xy on 2018/9/28.
 */
@Service
public class IdentifierServiceImpl implements IdentifierService {
    private static final String KEY = "product:identifier";

    @Resource
    private RedissonClient redissonClient;

    @Override
    public String build() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String nowString = simpleDateFormat.format(now);

        Integer seq = 1;
        RBucket<Integer> bucket = redissonClient.getBucket(KEY);
        if (bucket.isExists()) {
            seq = bucket.get() + 1;
        }

        bucket.set(seq);

        DateTime dateTime = new DateTime();
        dateTime = dateTime.plusDays(1);
        DateTime nextDay = new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), 0, 0, 0);
        bucket.expireAt(nextDay.getMillis());

        String format = String.format("%05d", seq);
        return nowString + format;
    }
}
