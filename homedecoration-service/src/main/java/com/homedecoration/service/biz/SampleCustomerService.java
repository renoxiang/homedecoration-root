package com.homedecoration.service.biz;

import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.CustomerRendering;
import com.homedecoration.persist.domain.biz.SampleCustomer;

import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
public interface SampleCustomerService {

    /**
     * 分页查询
     * @param expressions
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DomainPage selectByPage(List<Expression> expressions, Integer pageIndex, Integer pageSize);

    /**
     * 添加客户信息
     * @param sampleCustomer
     */
    void add(SampleCustomer sampleCustomer, List<CustomerRendering> renderingList);

    /**
     * 修改客户信息
     * @param sampleCustomer
     */
    void update(SampleCustomer sampleCustomer, List<CustomerRendering> renderingList);

    /**
     * 获取样板客户信息
     * @param sampleCustomerId
     * @return
     */
    SampleCustomer getById(Long sampleCustomerId);

    /**
     * 修改客户信息
     * @param sampleCustomer
     */
    void update(SampleCustomer sampleCustomer);
}
