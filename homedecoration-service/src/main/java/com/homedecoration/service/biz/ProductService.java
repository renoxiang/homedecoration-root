package com.homedecoration.service.biz;

import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.Product;

import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
public interface ProductService {

    /**
     * 分页查询
     * @param expressions
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DomainPage selectByPage(List<Expression> expressions, Integer pageIndex, Integer pageSize);

    /**
     * 添加产品
     * @param product
     */
    void add(Product product);

    /**
     * 修改产品
     * @param product
     */
    void update(Product product);

    /**
     * 获取产品信息
     * @param productId
     * @return
     */
    Product getById(Long productId);
}
