package com.homedecoration.service.biz.impl;

import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.ProductType;
import com.homedecoration.service.biz.ProductTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService {

    @Resource
    private BaseDao jpaBaseDao;
    @Override
    public List<ProductType> selectListByExpressions(List<Expression> expressions) {
        return jpaBaseDao.getEntitiesByExpressionList(ProductType.class, expressions);
    }

    @Override
    public ProductType getById(Long productTypeId) {
        return jpaBaseDao.getEntityById(ProductType.class, productTypeId);
    }

    @Override
    public DomainPage selectByPage(List<Expression> expressionList, Integer pageIndex, Integer pageSize) {
        return jpaBaseDao.getEntitiesPagesByExpressionList(ProductType.class, expressionList, pageIndex, pageSize);
    }

    @Transactional
    @Override
    public void add(ProductType productType) {
        jpaBaseDao.persist(productType);
    }

    @Transactional
    @Override
    public void update(ProductType productType) {
        ProductType target = getById(productType.getId());
        BeanUtil.merge(productType, target);
        jpaBaseDao.merge(target);
    }
}
