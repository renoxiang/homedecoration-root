package com.homedecoration.service.biz;

import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.ProductType;

import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
public interface ProductTypeService {

    List<ProductType> selectListByExpressions(List<Expression> expressions);

    ProductType getById(Long productTypeId);

    DomainPage selectByPage(List<Expression> expressionList, Integer pageIndex, Integer pageSize);

    void add(ProductType productType);

    void update(ProductType productType);
}
