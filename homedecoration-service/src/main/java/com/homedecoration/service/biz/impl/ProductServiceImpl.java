package com.homedecoration.service.biz.impl;

import com.biloba.common.core.util.BeanUtil;
import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Expression;
import com.biloba.common.domain.pojo.DomainPage;
import com.homedecoration.persist.domain.biz.Product;
import com.homedecoration.service.biz.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    private BaseDao jpaBaseDao;

    @Override
    public DomainPage selectByPage(List<Expression> expressions, Integer pageIndex, Integer pageSize) {
        return jpaBaseDao.getEntitiesPagesByExpressionList(Product.class, expressions, pageIndex, pageSize);
    }

    @Transactional
    @Override
    public void add(Product product) {
        jpaBaseDao.persist(product);
    }

    @Transactional
    @Override
    public void update(Product product) {
        Product target = getById(product.getId());
        BeanUtil.merge(product, target);
        jpaBaseDao.merge(target);
    }

    @Override
    public Product getById(Long productId) {
        return jpaBaseDao.getEntityById(Product.class, productId);
    }
}
