package com.homedecoration.service.biz.impl;

import com.biloba.common.dao.BaseDao;
import com.biloba.common.domain.Operation;
import com.homedecoration.persist.domain.biz.CustomerRendering;
import com.homedecoration.service.biz.CustomerRenderingService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by xy on 2018/9/28.
 */
@Service
public class CustomerRenderingServiceImpl implements CustomerRenderingService {
    @Resource
    private BaseDao jpaBaseDao;

    @Override
    public List<CustomerRendering> selectListById(Long customerId) {
        return jpaBaseDao.getEntitiesByField(CustomerRendering.class, "customerId", customerId);
    }
}
