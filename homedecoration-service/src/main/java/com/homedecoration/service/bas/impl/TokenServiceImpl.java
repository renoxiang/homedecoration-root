package com.homedecoration.service.bas.impl;

import com.biloba.common.core.util.UUIDUtil;
import com.biloba.common.dao.BaseDao;
import com.homedecoration.persist.domain.sys.Token;
import com.homedecoration.service.bas.TokenService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Description: TokenServiceImpl
 * @Author: estn.zuo
 * @CreateTime: 2014-10-30 15:05
 */
@CacheConfig(cacheNames = "cache:tianlu:TokenService")
@Service
public class TokenServiceImpl implements TokenService {

    @Resource
    private BaseDao jpaBaseDao;

    @Override
    public Token insert(Token token) {
        token.setToken(UUIDUtil.randomWithoutBar());
        token.setRefreshToken(UUIDUtil.randomWithoutBar());
        jpaBaseDao.persist(token);
        return token;
    }

    @Cacheable(key = "#token")
    @Override
    public Token retrieveToken(String token) {
        return jpaBaseDao.getEntityByField(Token.class, "token", token);
    }

    @Override
    public Token retrieveTokenByUid(Long userId) {
        return  jpaBaseDao.getEntityByField(Token.class, "targetId", userId);
    }

}
