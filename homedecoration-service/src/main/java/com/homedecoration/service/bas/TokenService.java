/**
 * Copyright  2014  Pemass
 * All Right Reserved.
 */

package com.homedecoration.service.bas;


import com.homedecoration.persist.domain.sys.Token;

/**
 * @Description: TokenService
 * @Author: estn.zuo
 * @CreateTime: 2014-10-30 15:04
 */
public interface TokenService {

    /**
     * 插入Token信息
     *
     * @param token
     * @return
     */
    Token insert(Token token);


    /**
     * 根据token检索Token
     *
     * @param token
     * @return
     */
    Token retrieveToken(String token);

    /**
     * 根据用户ID检索Token
     *
     * @param userId
     * @return
     */
    Token retrieveTokenByUid(Long userId);

}
