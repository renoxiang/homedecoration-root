package com.homedecoration.persist.domain.sys;

import com.biloba.common.domain.BaseDomain;
import com.homedecoration.persist.enumeration.OperatorStatusEnum;
import com.homedecoration.persist.enumeration.ProcessLevelEnum;

import javax.persistence.*;

/**
 * @Description: 操作员表
 * @Author: cassiel.liu
 * @CreateTime: 2017-05-04 14:24
 */
@Entity
@Table(name = "sys_operator")
public class Operator extends BaseDomain {

    @Column(name = "operatorname", length = 50, nullable = false)
    private String operatorname;    //操作员名称

    @Column(name = "password", length = 100, nullable = false)
    private String password;       //密码

    @Column(name = "role_id", nullable = false)
    private Long roleId;        //角色ID

    @Column(name = "nickname", length = 50, nullable = false)
    private String nickname;    //昵称

    @Column(name = "telephone", length = 50, nullable = false)
    private String telephone;    //联系电话

    @Column(name = "operator_status", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private OperatorStatusEnum operatorStatus; //账号状态

    @Column(name = "process_level")
    @Enumerated(EnumType.STRING)
    private ProcessLevelEnum processLevel;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "department_id")
    private Long departmentId;


    public String getOperatorname() {
        return operatorname;
    }

    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public OperatorStatusEnum getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(OperatorStatusEnum operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public ProcessLevelEnum getProcessLevel() {
        return processLevel;
    }

    public void setProcessLevel(ProcessLevelEnum processLevel) {
        this.processLevel = processLevel;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
}