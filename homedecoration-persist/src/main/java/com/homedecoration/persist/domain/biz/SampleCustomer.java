package com.homedecoration.persist.domain.biz;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 样板客户
 * Created by xy on 2018/9/28.
 */
@Entity
@Table(name = "biz_sample_customer")
public class SampleCustomer extends BaseDomain{
    /**
     * 客户名字
     */
    @Column(name = "customer_name", nullable = false, length = 50)
    private String customerName;

    /**
     * 客户编号
     */
    @Column(name = "customer_no", nullable = false, length = 20)
    private String customerNo;

    /**
     *客户地址
     */
    @Column(name = "location", nullable = false, length = 50)
    private String location;

    /**
     *设计概念
     */
    @Column(name = "concept", nullable = false, length = 20)
    private String concept;

    /**
     *客户介绍
     */
    @Column(name = "description", nullable = false, length = 200)
    private String description;

    /**
     *是否置顶（0-不置顶，1-置顶）
     */
    @Column(name = "is_top", nullable = false)
    private Integer isTop;

    /**
     * 预览图
     */
    @Column(name = "preview")
    private String preview;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
