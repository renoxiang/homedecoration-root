/**
 * Copyright  2014  Pemass
 * All Right Reserved.
 */

package com.homedecoration.persist.domain.sys;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Description: Token
 * @Author: estn.zuo
 * @CreateTime: 2014-10-30 14:59
 */
@Entity
@Table(name = "sys_token")
public class Token extends BaseDomain {

    @Column(name = "target_uuid", nullable = false, length = 32)
    private String targetUUID;  //目标对象UUID

    @Column(name = "device_id", nullable = false)
    private Long deviceId;  //设备

    @Column(name = "token", nullable = false, length = 32)
    private String token;   //token值

    @Column(name = "refresh_token", nullable = false, length = 32)
    private String refreshToken;    //刷新token值

    @Column(name = "target_id", nullable = false)
    private Long targetId;  //关联ID


    public String getTargetUUID() {
        return targetUUID;
    }

    public void setTargetUUID(String targetUUID) {
        this.targetUUID = targetUUID;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }
}
