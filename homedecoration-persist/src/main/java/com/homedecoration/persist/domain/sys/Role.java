package com.homedecoration.persist.domain.sys;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * User: yang
 * Date: 2017-06-06
 * Time: 10:08
 */
@Entity
@Table(name = "sys_role")
public class Role extends BaseDomain {
    @Column(name = "role_name",nullable = false)
    private String roleName;  //角色名称

    @Column(name = "remark",nullable = false)
    private String remark;  //备注

    @Column(name = "authority", length = 20, nullable = false)
    private String authority;//权限

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
