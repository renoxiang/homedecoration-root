package com.homedecoration.persist.domain.biz;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 产品
 * Created by xy on 2018/9/28.
 */
@Entity
@Table(name = "biz_product")
public class Product extends BaseDomain {

    /**
     * 产品名称
     */
    @Column(name = "product_name", nullable = false, length = 50)
    private String productName;

    /**
     * 产品编号
     */
    @Column(name = "product_no", nullable = false, length = 20)
    private String productNo;

    /**
     * 生产商
     */
    @Column(name = "company", nullable = false, length = 50)
    private String company;

    /**
     * 型号
     */
    @Column(name = "model", nullable = false)
    private String model;

    /**
     * 类型id
     */
    @Column(name = "type_id", nullable = false)
    private Long typeId;

    /**
     *是否置顶（0-不置顶，1-置顶）
     */
    @Column(name = "is_top", nullable = false)
    private Integer isTop;

    /**
     * 价格
     */
    @Column(name = "price")
    private BigDecimal price;

    /**
     * 描述
     */
    @Column(name = "description", nullable = false, length = 200)
    private String description;

    /**
     * 产品介绍（html5页面地址）
     */
    @Column(name = "html_url")
    private String htmlUrl;

    /**
     * 预览图
     */
    @Column(name = "preview")
    private String preview;

    /**
     * 产品图集
     * （url地址，中间以,分割）
     */
    @Column(name = "images")
    private String images;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
