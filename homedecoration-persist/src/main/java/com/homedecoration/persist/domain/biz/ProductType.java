package com.homedecoration.persist.domain.biz;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 产品类型
 * Created by xy on 2018/9/28.
 */
@Entity
@Table(name = "biz_product_type")
public class ProductType extends BaseDomain
{
    /**
     * 类型名称
     */
    @Column(name = "name", nullable = false, length = 20)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
