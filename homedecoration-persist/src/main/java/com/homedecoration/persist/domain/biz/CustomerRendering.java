package com.homedecoration.persist.domain.biz;

import com.biloba.common.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 样板客户-效果图
 * Created by xy on 2018/9/28.
 */
@Entity
@Table(name = "biz_customer_rendering")
public class CustomerRendering extends BaseDomain{

    /**
     * 客户id
     */
    @Column(name = "customer_id")
    private Long customerId;

    /**
     * 预览图
     */
    @Column(name = "preview", nullable = false)
    private String preview;

    /**
     * 描述
     */
    @Column(name = "description", length = 50)
    private String description;


    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
