/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.homedecoration.persist.vo;

/**
 * @Description: ConfigVO
 * @Author: estn.zuo
 * @CreateTime: 2017-03-27 16:44
 */
public class ConfigVO {

    private String key; //key

    private String value;   //值

    private String title;   //标题

    private Integer isSwitch;  //开关 针对url

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIsSwitch() {
        return isSwitch;
    }

    public void setIsSwitch(Integer isSwitch) {
        this.isSwitch = isSwitch;
    }
}
