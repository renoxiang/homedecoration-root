/**
 * Copyright  2014  Pemass
 * All Right Reserved.
 */

package com.homedecoration.persist.vo;


import com.biloba.common.core.serializer.ResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @Description: MessagePojo
 * @Author: estn.zuo
 * @CreateTime: 2014-12-23 10:11
 */
public class ResourcesVO {

    @JsonSerialize(using = ResourceUrlSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
    private String url;//图片URL

    private String relativeURL;  //图片相对地址

    public ResourcesVO() {
    }

    public ResourcesVO(String url) {
        this.url = url;
        this.relativeURL = url;
    }

    public String getRelativeURL() {
        return relativeURL;
    }

    public void setRelativeURL(String relativeURL) {
        this.relativeURL = relativeURL;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
