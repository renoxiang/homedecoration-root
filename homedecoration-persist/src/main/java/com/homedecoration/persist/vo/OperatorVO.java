package com.homedecoration.persist.vo;


import com.homedecoration.persist.enumeration.OperatorStatusEnum;

public class OperatorVO {

    private Long id; //id

    private String operatorname;    //操作员名称

    private String password;       //密码

    private String role;        //角色

    private String nickname;    //昵称

    private String telephone;    //联系电话

    private OperatorStatusEnum operatorStatus; //角色状态


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperatorname() {
        return operatorname;
    }

    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public OperatorStatusEnum getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(OperatorStatusEnum operatorStatus) {
        this.operatorStatus = operatorStatus;
    }
}
