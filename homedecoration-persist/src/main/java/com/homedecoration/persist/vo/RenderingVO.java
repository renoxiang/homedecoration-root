package com.homedecoration.persist.vo;

import com.homedecoration.persist.domain.biz.CustomerRendering;

import java.util.List;

/**
 * Created by xy on 2018/9/30.
 */
public class RenderingVO {
    List<CustomerRendering> rendering;

    public List<CustomerRendering> getRendering() {
        return rendering;
    }

    public void setRendering(List<CustomerRendering> rendering) {
        this.rendering = rendering;
    }
}
