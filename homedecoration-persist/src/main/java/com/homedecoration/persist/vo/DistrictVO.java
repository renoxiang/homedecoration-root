package com.homedecoration.persist.vo;

/**
 * Created by xy on 2017/8/22.
 */
public class DistrictVO {

    private Integer id;

    private String districtName;

    public DistrictVO(Integer id, String districtName) {
        this.id = id;
        this.districtName = districtName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}
