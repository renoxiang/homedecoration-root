/**
 * Copyright  2017  Pemass
 * All Right Reserved.
 */
package com.homedecoration.persist.vo;

import java.util.Date;

/**
 * @Description: UserVO
 * @Author: xy
 * @CreateTime: 2017-06-20 16:03
 */
public class UserVO {

    private Long id;

    private String username;

    private String nikename;

    private Date createTime;

    private Integer reportAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNikename() {
        return nikename;
    }

    public void setNikename(String nikename) {
        this.nikename = nikename;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getReportAmount() {
        return reportAmount;
    }

    public void setReportAmount(Integer reportAmount) {
        this.reportAmount = reportAmount;
    }
}