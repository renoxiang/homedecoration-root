package com.homedecoration.persist.dao.sys;

import com.homedecoration.persist.domain.sys.Operator;

public interface OperatorDao {

    Operator selectAllOperatorByName(String operatorname);
}
