package com.homedecoration.persist.dao.sys.impl;

import com.homedecoration.persist.dao.JpaBaseDao;
import com.homedecoration.persist.dao.sys.OperatorDao;
import com.homedecoration.persist.domain.sys.Operator;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;


@Repository
public class OperatorDaoImpl extends JpaBaseDao implements OperatorDao {
    @Override
    public Operator selectAllOperatorByName(String operatorname) {

        StringBuffer sql = new StringBuffer();
        sql.append("select * from sys_operator where operatorname = ?0 order by update_time desc");
        Query query = getEntityManager().createNativeQuery(sql.toString(), Operator.class);
        query.setParameter(0, operatorname);

        List<Operator> list = query.getResultList();
        if(list == null || list.size() == 0){
            return null;
        }
        return list.get(0);
    }
}
