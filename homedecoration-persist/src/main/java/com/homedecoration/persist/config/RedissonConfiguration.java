package com.homedecoration.persist.config;

import com.biloba.common.dao.cache.RedissonSpringCacheManager;
import com.google.common.collect.Maps;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.redisson.spring.cache.CacheConfig;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Description: Redisson配置
 * @Author: cassiel.liu
 * @CreateTime: 2017-04-15 15:55
 */

@Configuration
@EnableCaching(proxyTargetClass = true)
@EnableConfigurationProperties(RedisProperties.class)
public class RedissonConfiguration {

    @Resource
    private RedisProperties redisProperties;

    /**
     * 生成通用的Redisson客户端
     *
     * @return
     */
    @Bean
    public RedissonClient redissonClient() {
        Config config = getConfig(redisProperties.getDatabase());
        return Redisson.create(config);
    }

    /**
     * Redis缓存
     *
     * @return
     */
    @Bean
    public CacheManager cacheManager(RedissonClient redissonClient) {
        Map<String, CacheConfig> _config = Maps.newHashMap();
        Long ONE_MINUTE = 1000 * 60L;
        _config.put("cache\\S*", new CacheConfig(ONE_MINUTE * 60, 0));
        return new RedissonSpringCacheManager(redissonClient, _config);
    }

    /**
     * @param database 数据库编号
     * @return
     */
    private Config getConfig(int database) {
        Config config = new Config();
        //sentinel
        if (redisProperties.getSentinel() != null) {
            SentinelServersConfig sentinelServersConfig = config.useSentinelServers();
            sentinelServersConfig.setMasterName(redisProperties.getSentinel().getMaster());
            redisProperties.getSentinel().getNodes();
            sentinelServersConfig.addSentinelAddress(redisProperties.getSentinel().getNodes().split(","));
            sentinelServersConfig.setDatabase(database);
            if (redisProperties.getPassword() != null) {
                sentinelServersConfig.setPassword(redisProperties.getPassword());
            }
        } else { //single server
            SingleServerConfig singleServerConfig = config.useSingleServer();
            singleServerConfig.setAddress(redisProperties.getHost() + ":" + redisProperties.getPort());
            singleServerConfig.setDatabase(database);
            if (redisProperties.getPassword() != null) {
                singleServerConfig.setPassword(redisProperties.getPassword());
            }
        }
        return config;
    }
}