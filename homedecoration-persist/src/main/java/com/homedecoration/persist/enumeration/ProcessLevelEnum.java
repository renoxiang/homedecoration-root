package com.homedecoration.persist.enumeration;

/**
 * Created by xy on 2017/8/4.
 */
public enum ProcessLevelEnum {

    DISTRICT("区县处理员"),

    DEPARTMENT("部门处理员"),;

    private String description;

    ProcessLevelEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
