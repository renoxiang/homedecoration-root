package com.homedecoration.persist.enumeration;

public enum OperatorStatusEnum {
    FROZEN("冻结"),

    ACTIVATION("激活"),;

    private String description;

    OperatorStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
